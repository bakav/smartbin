﻿using Hackathon.SmartBin.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hackathon.SmartBin.Domain
{
    public class TaskDomain : DomainBase
    {
        public TaskDomain(string binID)
        {
            this.SmartBinID = binID;
        }
        public string SmartBinID { get; set; }

        public string Title { set; get; }

        public string Body { set; get; }

        public string HandlerID { get; set; }

        public TaskStatusEnum Status { get; set; }

        public void FinishTask()
        {
            this.Status = TaskStatusEnum.Finished;
        }

        public bool InsertTask(string binID)
        {
            this.SmartBinID = binID;
            Title = Constants.title;

            //save to db
            return true;
        }
    }
}
