﻿using Hackathon.SmartBin.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hackathon.SmartBin.Domain
{
    public class SmartBinDomain:DomainBase
    {
        public string BinID { set; get; }

        public string Name { set; get; }

        public string No { set; get; }

        public decimal Lat { set; get; }

        public decimal Ing{ set; get; }

        public string Address { set; get; }

        public string Region { set; get; }

        public string Cleaner { get; set; }

        public List<SubBin> ListSubBin { get; set; }

        public bool IsNotifyCleaner { get; set; }

        public bool IsNotified { get; set; }       
        

        /// <summary>
        /// update current smart bin status
        /// </summary>
        /// <param name="currentListSub"></param>
        public void UpdateCurrentStatus(List<SubBin> currentListSub)
        {
            //update all sub bins status
            for(int i=0;i< ListSubBin.Count;i++)
            {
                ListSubBin[i].UsedCapacity = currentListSub[i].Capacity;

                if (ListSubBin[i].UsedCapacity > Constants.RedWarningRatio * ListSubBin[i].Capacity)
                {
                    ListSubBin[i].Color = ColorEnum.Red;
                    IsNotifyCleaner = true;
                }
                else if (ListSubBin[i].UsedCapacity > Constants.OrangeWarningRatio * ListSubBin[i].Capacity)
                {
                    ListSubBin[i].Color = ColorEnum.Orange;
                }
                else
                {
                    ListSubBin[i].Color = ColorEnum.Green;
                    IsNotifyCleaner = false;
                    IsNotified = false;

                    //if time is suffient, we can send a rabbit mq to task domain, here we just invoke directly
                    TaskDomain task = new TaskDomain(BinID);

                    //since current smart bin status is green color, if havig task, will be closed
                    if (task != null)
                    {
                        task.FinishTask();
                    }
                }
            }
        }


    }

    public class SubBin
    {
        public string ID { get; set; }
        public CatagoryEnum Catagory
        { set; get; }

        public decimal Capacity { get; set; }

        public decimal UsedCapacity { get; set; }

        public ColorEnum Color { get; set; }
    }
}
