﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hackathon.SmartBin.Domain
{
    public class CleanerDomain
    {
        public string Name { get; set; }

        public string Phone { get; set; }
    }
}
