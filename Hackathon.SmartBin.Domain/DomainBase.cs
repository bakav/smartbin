﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hackathon.SmartBin.Domain
{
    public class DomainBase
    {
        public string CreateBy { get; set; }
        public DateTime CreateTime { get; set; }
        public string LastModifyBy { get; set; }
        public DateTime LastModifyTime { get; set; }
    }
}
