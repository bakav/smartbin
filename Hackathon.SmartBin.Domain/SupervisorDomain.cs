﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hackathon.SmartBin.Domain
{
    public class SupervisorDomain : DomainBase
    {
        public string Name { get; set; }

        public string Phone { get; set; }

        public bool AssignTask(string binID)
        {
            //if time is suffient, we can send a rabbit mq to task domain, here we just invoke directly
            TaskDomain task = new TaskDomain(binID);
            task.InsertTask(binID);

            return true;
        }
             
    }
}
