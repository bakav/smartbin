﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hackathon.SmartBin.Domain
{
    public class UserDomain
    {
        public string UserID { get; set; }

        public byte[] photo { get; set; }

        public string UserName { get; set; }

        public string Level { get; set; }

        public string Phone { get; set; }

        public string Point { get; set; }

       

        public List<UserDomain> fancyList
        {
            get;set;
        }

        public List<UserDomain> InterestList
        {
            get; set;
        }

        public UserDomain()
        {
            fancyList=new  List<UserDomain>();
            InterestList= new  List<UserDomain>();
        }

        public void AddFancy(UserDomain user)
        {
            fancyList.Add(user);
        }

        public void AddInterest(UserDomain user)
        {
            InterestList.Add(user);
        }

       
    }
}
