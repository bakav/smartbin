﻿using Hackathon.SmartBin.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hackathon.SmartBin.DAL
{
    public class SmartBinDAL
    {
        public static List<SmartBinDomain> GetSmartBinListByRegion(string regionID)
        {

            List<SmartBinDomain> list = new List<SmartBinDomain>();
            //just some sample data, not from db
            SmartBinDomain domain = new SmartBinDomain();
            domain.BinID = "0101";
            domain.Region = "姑苏区";
            domain.Name = "智能垃圾箱";
            domain.Address = "观前街巷子口";
            domain.No = "3";
            domain.Lat = 232.3M;
            domain.Ing = 345M;
            list.Add(domain);

            SmartBinDomain domain1 = new SmartBinDomain();
            domain1.BinID = "0101";
            domain1.Region = "姑苏区";
            domain1.Name = "智能垃圾箱";
            domain1.Address = "观前街巷子口";
            domain1.No = "3";
            domain1.Lat = 232.3M;
            domain1.Ing = 345M;
            list.Add(domain1);

            return list;
        }

        public static SmartBinDomain GetBinDetailedInfo(string binID)
        {
            //just some sample data, not from db
            //just some sample data, not from db
            SmartBinDomain domain = new SmartBinDomain();
            domain.BinID = "0101";
            domain.Region = "姑苏区";
            domain.Name = "智能垃圾箱";
            domain.Address = "观前街巷子口";
            domain.No = "3";
            domain.Lat = 232.3M;
            domain.Ing = 345M;

            SubBin sub = new SubBin();
            sub.Capacity = 53M;
            sub.Catagory = Common.CatagoryEnum.Recyclable;
            sub.UsedCapacity = 15M;
            sub.Color = Common.ColorEnum.Green;
            domain.ListSubBin.Add(sub);

            SubBin sub1 = new SubBin();
            sub1.Capacity = 53M;
            sub1.Catagory = Common.CatagoryEnum.Kitchen;
            sub1.UsedCapacity = 15M;
            sub1.Color = Common.ColorEnum.Orange;
            domain.ListSubBin.Add(sub1);

            SubBin sub2 = new SubBin();
            sub2.Capacity = 53M;
            sub2.Catagory = Common.CatagoryEnum.Harmful;
            sub2.UsedCapacity = 15M;
            sub2.Color = Common.ColorEnum.Orange;
            domain.ListSubBin.Add(sub2);

            SubBin sub3 = new SubBin();
            sub3.Capacity = 53M;
            sub3.Catagory = Common.CatagoryEnum.Other;
            sub3.UsedCapacity = 15M;
            sub3.Color = Common.ColorEnum.Orange;
            domain.ListSubBin.Add(sub3);

            return domain;
        }
    }
}