﻿using Hackathon.SmartBin.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hackathon.SmartBin.DAL
{
    public class UserDAL
    {
        public  static UserDomain GetUserByID(string userID)
        {
            UserDomain user = new UserDomain();
            user.Level = "7";
            user.photo = new byte[10];
            user.Point = "35";
            user.UserName = "李明";
            user.UserID = "23566";

            //add user fancy
            UserDomain fan = new UserDomain();
            fan.UserID = "353";
            user.fancyList.Add(fan);
            UserDomain fan1 = new UserDomain();
            fan1.UserID = "232";
            user.fancyList.Add(fan1);

            //add user interest
            UserDomain interest = new UserDomain();
            interest.UserID = "353";
            user.InterestList.Add(interest);
            UserDomain interest1 = new UserDomain();
            interest1.UserID = "222";
            user.InterestList.Add(interest1);

            return user;
        }
    }
}
