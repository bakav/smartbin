﻿using Hackathon.SmartBin.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hackathon.SmartBin.DAL
{
    public class CollectionActivityDAL
    {
        public static List<CollectionActivityDomain> GetCollectionActivitiesByUser(string userID)
        {
            List<CollectionActivityDomain> list = new List<CollectionActivityDomain>();
            //just some sample data, not from db
            CollectionActivityDomain domain = new CollectionActivityDomain();
            domain.BinID = "0101";
            domain.BinName = "智能垃圾箱";
            domain.SubBinID = "010101";
            domain.Weight = 0.8M;
            domain.BinAddresss = "观前街巷子口";
            domain.BinNo = "3";
            domain.CreateTime= DateTime.Now.AddDays(-3);
            list.Add(domain);

            CollectionActivityDomain domain1 = new CollectionActivityDomain();
            domain1.BinID = "0101";
            domain1.SubBinID = "010101";
            domain1.Weight = 1.2M;
            domain1.BinName = "智能垃圾箱";
            domain1.BinAddresss = "十全街";
            domain1.BinNo = "1";
            domain1.CreateTime = DateTime.Now.AddDays(-2);
            list.Add(domain1);

            return list;
        }
    }
}
