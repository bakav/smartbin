﻿using Hackathon.SmartBin.Common;
using Newtonsoft.Json.Linq;

namespace Hackathon.SmartBin.Business
{

    public class GarbageClassifyService
    {
        
        public static JObject Recognize(byte[] image)
        {
            var client = new Baidu.Aip.ImageClassify.ImageClassify(Constants.BaiduAIAPIKey, Constants.BaiduAISecretKey);

            var result = client.AdvancedGeneral(image);
            return result;
        }
    }
}
