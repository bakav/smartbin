﻿using Hackathon.SmartBin.DAL;
using Hackathon.SmartBin.Domain;
using Hackathon.SmartBin.Dto;
using System.Collections.Generic;
using System.Linq;

namespace Hackathon.SmartBin.Business
{
    public class SmartBinService
    {
        public static List<SmartBinDto> GetSmartBins(string areaCode)
        {
            var list = SmartBinDAL.GetSmartBinListByRegion(areaCode);
            if (list != null)
            {
                return list.Select(m => new SmartBinDto
                {

                    ID = m.BinID,
                    Number = m.No,
                    Status = m.IsNotifyCleaner ? 1 : 0
                }).ToList();
            }

            return null;
        }

        public static bool NotifyToClean(string id)
        {
            var bin= SmartBinDAL.GetBinDetailedInfo(id);

            if (bin!=null)
            {
                SupervisorDomain supervisor= new  SupervisorDomain();
                return supervisor.AssignTask(bin.BinID);
            }
            
            return false;
        }
    

}
}
