﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hackathon.SmartBin.Dto
{
    public class UserDto
    {

        public string UserID { get; set; }

        public string Photo { get; set; }

        public string UserName { get; set; }

        public string Level { get; set; }

        public string Phone { get; set; }

        public string Point { get; set; }

        public int FancyListCount
        {
            get; set;
        }

        public int InterestListCount
        {
            get; set;
        }


        public  IEnumerable<CollectionActivityDto> Activities { get; set; }

    }
}
