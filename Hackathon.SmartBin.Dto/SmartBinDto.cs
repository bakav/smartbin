﻿namespace Hackathon.SmartBin.Dto
{
    public class SmartBinDto
    {
        public string ID { get; set; }

        public string Number { get; set; }

        public int Status { get; set; }
    }
}
