﻿namespace Hackathon.SmartBin.Dto
{
    public class CollectionActivityDto
    {

        public string Address { get; set; }

        public  string BinNo { get; set; }
        public string Date { get; set; }


        public decimal Weight { get; set; }
    }
}
