﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Hackathon.SmartBin.Dto
{
    public class SubBinDto
    {
        [JsonProperty("capacity")]
        public double Capacity { get; set; }
    }
}
