﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Hackathon.SmartBin.Dto
{
    public class RecognizeResultDto
    {
        public string Cagetory { get; set; }

        public JObject Result { get; set; }
    }
}
