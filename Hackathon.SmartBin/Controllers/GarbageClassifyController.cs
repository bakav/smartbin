﻿
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Hackathon.SmartBin.Business;
using Hackathon.SmartBin.Common;
using Hackathon.SmartBin.Dto;

namespace Hackathon.SmartBin.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class GarbageClassifyController : ControllerBase
    {
        /// <summary>
        /// classify garbage category
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        [HttpPost("recognize")]
        public RecognizeResultDto Recognize(ImageDto image)
        {

            if (image != null && !string.IsNullOrEmpty(image.Data))
            {
                string data = image.Data;
                if (image.Data.StartsWith("data:image/jpeg;base64,"))
                {
                    data = image.Data.Replace("data:image/jpeg;base64,", "");
                }
                
                var bytes = Convert.FromBase64String(data);
                return new  RecognizeResultDto()
                {
                    Result = GarbageClassifyService.Recognize(bytes),
                    Cagetory = CatagoryEnum.Recyclable.ToString()
                };
            }

            return null;
        }
    }
}
