﻿
using Hackathon.SmartBin.Business;
using Hackathon.SmartBin.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hackathon.SmartBin.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        [HttpGet("{id}")]
        public UserDto Get(string id)
        {
            return UserService.GetUser(id);
        }
    }
}
