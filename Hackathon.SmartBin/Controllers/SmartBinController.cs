﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hackathon.SmartBin;
using Hackathon.SmartBin.Business;
using Hackathon.SmartBin.Dto;
using Microsoft.AspNetCore.Mvc;

namespace Hackathon.SmartBin.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SmartBinController : ControllerBase
    {
        [HttpGet("areas/{areaCode}")]
        public IEnumerable<SmartBinDto> GetSmartBins(string areaCode)
        {

            return SmartBinService.GetSmartBins(areaCode);
        }


        [HttpGet("{id}")]
        public SmartBinDto Get(string id)
        {
            return null;
        }

        [HttpPut("{id}")]
        public bool SyncStatus([FromBody] SmartBinDto smartBinDto,string id)
        {
            return true;
        }

        [HttpPost("clean/{id}")]
        public bool Clean(string id)
        {
            return SmartBinService.NotifyToClean(id);
        }
    }
}
