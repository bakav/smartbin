import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, HashRouter, Route, Switch } from 'react-router-dom';
import './index.css';
import Ipad from '../src/components/Ipad';
import IphoneFindBin from '../src/components/Iphone/FindBin';
import IphoneUser from '../src/components/Iphone/User';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Ipad} />
      <Route exact path="/iphone" component={IphoneUser} />
      <Route exact path="/iphone-find-bin" component={IphoneFindBin} />
    </Switch>
  </BrowserRouter>
  , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
