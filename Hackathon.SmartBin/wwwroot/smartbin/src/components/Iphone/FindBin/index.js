import React, { Component } from 'react';
import IphoneFindBinHeader from '../../widgets/IphoneFindBinHeader';
import map from '../../../static/map.png';
import './index.scss';

const IphoneFindBin = () => (
  <div className="iphone-find-bin">
    <IphoneFindBinHeader title="查找附近的垃圾桶"/>

    <div className="iphone-find-bin_main">
      <img src={map} alt="map" className="map"/>
    </div>
  </div>
);

export default IphoneFindBin;