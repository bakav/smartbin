import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import IphoneUserHeader from '../../widgets/IphoneUserHeader';
import Collapse from '../../widgets/Collapse';
import FindBin from '../FindBin';

import recycle from '../../../static/recycle.png';
import bin from '../../../static/bin.png';
import './index.scss';

class IphoneUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      disable: true,
    };
    this.collapseHandler = this.collapseHandler.bind(this);
  }

  collapseHandler() {
    this.setState({
      disable: !this.state.disable,
    })
  }

  findBin() {
    return <FindBin />
  }

  render() {
    return (
      <div className="iphone-user">
        <IphoneUserHeader />

        <div className="iphone-user_main">
          <Collapse image={recycle} name="回收记录" onClick={this.collapseHandler} disable={this.state.disable}/>
          <Link to="/iphone-find-bin"><Collapse image={bin} name="查找附近的垃圾桶" onClick={this.findBin} disable="true"/></Link>
        </div>
        
        <div className="log-out">
          <button>退出登录</button>
        </div>
      </div>
    );
  }
}

export default IphoneUser;