import React from 'react';
import iconMenu from '../../../static/icon-menu.png';
import iconUser from '../../../static/icon-user.png';
import iconExit from '../../../static/icon-exit.png';
import './index.scss';

const Header = () => (
  <div className='header-container'>
    <div className='left-header'>
      <img alt='icon-menu' src={iconMenu} />
    </div>
    <div className='right-header'>
      <div className='header-title'>智能垃圾清理回收后台管理系统</div>
      <div className='header-button-container'>
        <img alt='user' src={iconUser} />
        <img alt='exit' src={iconExit} />
      </div>
    </div>
  </div>
)

export default Header;