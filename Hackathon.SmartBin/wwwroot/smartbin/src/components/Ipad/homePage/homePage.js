import React from 'react';
import iconActive from '../../../static/icon-active.png';
import SmartBin from '../../widgets/smartBin/index';
import './index.scss';


function getBinData() {
  return [
    {id:'001', percentage:'50%'},
    {id:'002', percentage:'80%'},
    {id:'003', percentage:'20%'},
    {id:'004', percentage:'70%'},
  ]
}

const HomePage = (() => (
  <div className='bin-container'>
    <div className='current-status'>
      <img alt='icon-active' src={iconActive} />
      星湖街道垃圾清理实时状态
    </div>
    <div className='smart-bin-container'>
      {getBinData().map(item => (
        <SmartBin
          key={item.id}
          id={item.id}
          percentage={item.percentage}
        />
      )
      )}
    </div>
  </div>
));

export default HomePage;