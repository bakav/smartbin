import { BrowserRouter as Router, Route, NavLink } from 'react-router-dom';
import React from 'react';
import CurrentLocation from '../../widgets/currentLocation/index';
import HomePage from '../../Ipad/homePage/homePage';
import iconHome from '../../../static/icon-home.png';
import iconStatic from '../../../static/icon-static.png';
import iconRecord from '../../../static/icon-record.png';
import iconReminder from '../../../static/icon-reminder.png';
import iconNavigation from '../../../static/icon-navigation.png';
import ipadMap from '../../../static/ipad-map.png';
import './index.scss';

function getRouteDetail() {
  return [
    { path: '/', component: Home, src: iconHome },
    { path: '/static', component: NotImplemented, src: iconStatic },
    { path: '/record', component: NotImplemented, src: iconRecord },
    { path: '/reminder', component: NotImplemented, src: iconReminder },
    { path: '/navigation', component: Navigation, src: iconNavigation },
  ];
}

const RouterMenu = () => (
  <Router>
    <div className='main-container'>
      <div className='router-container'>
        <ul>
          {getRouteDetail().map(icon => (
            <React.Fragment>
              <li>
                <NavLink to={icon.path}>
                  <img alt={icon.src} src={icon.src} />
                </NavLink>
              </li>
              <hr />
            </React.Fragment>
          ))
          }
        </ul>
      </div>
      {
        getRouteDetail().map(item => (
          <Route exact path={item.path} component={item.component} />
        ))
      }
    </div>
  </Router>
);

const Home = () => (
  <div className='content-contaniner'>
    <h2>
      <CurrentLocation
        location="管理区域"
      />
    </h2>
    <HomePage />
  </div>
);

const NotImplemented = () => (
  <div className='content-contaniner'>
    <h2>
      This page is not implemented yet!
    </h2>
  </div>
);

const Navigation = () => (
  <div className='content-contaniner'>
    <h2>
      <CurrentLocation
        location="查找附近的垃圾桶"
      />
    </h2>
    <img src={ipadMap} alt="map" className="map"/>
  </div>
);


export default RouterMenu;