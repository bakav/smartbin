import React from 'react';
import RouterMenu from '../Ipad/router/router';
import Header from '../Ipad/header/index';

const Ipad = () => (
  <React.Fragment>
    <Header />
    <RouterMenu />
  </React.Fragment>
);

export default Ipad;