import React from 'react';
import IphoneUserActivity from '../IphoneUserActivity';

import avatar from '../../../static/avatar.png';
import share from '../../../static/share.png';
import './index.scss';

const IphoneUserHeader = () => (
  <header className="iphone-user_header">
    <div className="iphone-user_info">
      <img className="iphone-user-avatar" src={avatar} alt="avatar" />
      
      <div className="iphone-user-statistics">
        <p>张惠文</p>

        <div className="iphone-user-level">Lv.7</div>
        <div className="iphone-user-points">50贡献值</div>
      </div>

      <img className="iphone-user_share" src={share} alt="share"/>
    </div>

    <div className="iphone-user_activities">
      <IphoneUserActivity number="30" name="动态"/>
      <IphoneUserActivity number="11" name="我的关注"/>
      <IphoneUserActivity number="22" name="我的粉丝"/>
      <IphoneUserActivity number="15" name="回收记录"/>
    </div>
  </header>
);

export default IphoneUserHeader;