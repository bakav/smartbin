import React from 'react';

import rightArrow from '../../../static/right-arrow.png';
import downArrow from '../../../static/down-arrow.png';
import './index.scss';

const Collapse = ({image, name, onClick, disable}) => (
  <div className="iphone-user_collapse" onClick={onClick}>
    <div className="iphone-user_collapse-title">
      <img src={image} alt="" className="iphone-user_icon"/>
      <p>{name}</p>

      <img src={disable?rightArrow:downArrow} alt="" className={disable?"iphone-user_right-arrow":"iphone-user_down-arrow"}/>
    </div>

    <div className={disable?"iphone-user_collapse-content-disabled":"iphone-user_collapse-content"}>
      <span>
        星湖街23号09号垃圾桶清理垃圾0.8kg
      </span>

      <span className="date">
        2018/09/28
      </span>
    </div>
  </div>
  
);

export default Collapse;