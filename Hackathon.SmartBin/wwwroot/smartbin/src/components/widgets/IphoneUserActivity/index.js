import React from 'react';

import './index.scss';

const IphoneUserActivity = ({number, name}) => (
  <div className="iphone-user_activity">
    <p className="iphone-user_activity-number">{number}</p>
    <p className="iphone-user_activity-name">{name}</p>
  </div>
);

export default IphoneUserActivity;