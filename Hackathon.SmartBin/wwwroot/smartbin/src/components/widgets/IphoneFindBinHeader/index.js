import React from 'react';
import { Link } from 'react-router-dom';
import arrow from '../../../static/arrow.png';

import './index.scss';

const IphoneFindBinHeader = ({title}) => (
  <header className="iphone-find-bin_header">
    <Link to="/iphone-user"><img src={arrow} alt="arrow"/></Link>
    <div>{title}</div>
  </header>
);

export default IphoneFindBinHeader;