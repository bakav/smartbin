import React from 'react';
import smartBinPic from '../../../static/smart-bin.png';
import './index.scss';

const SmartBin = ({id,percentage}) => (
  <div className='smart-bin'>
    <img alt='smartbin' src={smartBinPic} />
    <span className='bin-id'>
      {id}
    </span>
    <span className='bin-percentage'>{percentage}</span>
  </div>
);

export default SmartBin;