import React from 'react';
import './index.scss';


const CurrentLocation = ({location}) => (
  <div className='current-location'>
    当前位置：
    <span>
      {location}
    </span>
  </div>
);

export default CurrentLocation;